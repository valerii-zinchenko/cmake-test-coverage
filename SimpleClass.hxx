#ifndef SIMPLECLASS_H
#define SIMPLECLASS_H

struct SimpleClass {
	SimpleClass();
	~SimpleClass();

	int add(int a, int b);
	int multiply(int a, int b);
};

#endif
