#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "../SimpleClass.hxx"

/**
 * By playing with code coverage - try comment, uncomment or add few test, or remove few tests.
 * This is needed, because sometimes running "lcov" to generate code coverage information it is crashing for some uncnown reasons with the following exception:
geninfo: ERROR: {path_to_the_source}/CMakeFiles/test_coverage.dir/tests/main_test.cxx.gcno: reached unexpected end of file
 *
 * By undoing the change it "lcov" works without problems.
 * It is maybe related to lcov v1.10.0, which was installed on my Ubuntu. After manual instalation of v1.13 that issue disappear
 */

TEST_CASE("Simple operations") {
	SimpleClass sc;

	SECTION("adding") {
		REQUIRE(sc.add(1,1) == 2);
		REQUIRE(sc.add(4,7) == 11);
	}
}
