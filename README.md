# Goals

Mainly everything is related to `cmake` and how to setup development environment. So here I am trying to solve the following tasks:

* how to define external dependecies and install them without affecting the system
* how to connect testing framework, add tests and generate code coverage report

## Requirements

The following tools need to be installed
* cmake 3.5.1+
* git
* lcov (min 1.10.0, but newer version is recommended)
* gcov
* genhtml

It is recommended newer version of `lcov` (1.13), because I faced odd unstability of 1.10 - small tweak in test file is crashing the generating of code coverage report. Sometimes it is working without problem, but sometimes, for example after adding a new asserting line and comment it out, it is craching with the following error:

```
geninfo: ERROR: /home/..../CMakeFiles/test_coverage.dir/tests/main_test.cxx.gcno: reached unexpected end of file
```

## Run

```
cmake .
make coverage
```

Then open `coverage/index.html`
