#include "SimpleClass.hxx"

SimpleClass::SimpleClass() {}

SimpleClass::~SimpleClass() {}

int SimpleClass::add(int a, int b) {
	return a + b;
}

int SimpleClass::multiply(int a, int b) {
	int result;

	if (b == a) {
		result = a*a;
	} else
		result = a * b;

	return result;
}
